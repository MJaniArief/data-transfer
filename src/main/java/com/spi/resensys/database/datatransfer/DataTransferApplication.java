package com.spi.resensys.database.datatransfer;

import com.spi.resensys.database.datatransfer.models.ResensysData;
import com.spi.resensys.database.datatransfer.models.ResensysFormat;
import com.spi.resensys.database.datatransfer.models.ResensysHistory;
import com.spi.resensys.database.datatransfer.models.ResensysInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

@SpringBootApplication
@EnableScheduling
@EnableTransactionManagement
@EnableJpaRepositories(
		entityManagerFactoryRef = "targetInfoEntityManager",
		transactionManagerRef = "targetInfoTransactionManager",
		basePackages = "com.spi.resensys.database.datatransfer.repositories"
)
public class DataTransferApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataTransferApplication.class, args);
	}

	// SOURCE CONFIGURATION
	@Bean
	@Autowired
	public JdbcTemplate source(@Qualifier("sourceDataSource") DataSource sourceDataSource) {
		return new JdbcTemplate(sourceDataSource);
	}

	@Bean
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource sourceDataSource() {
		return DataSourceBuilder
				.create()
				.build();
	}

	// TARGET CONFIGURATION
	@Bean
	@Autowired
	@Primary
	public JdbcTemplate target(@Qualifier("targetDataSource") DataSource targetDataSource) {
		return new JdbcTemplate(targetDataSource);
	}

	@Bean
	@ConfigurationProperties(prefix = "spring.datatarget")
	@Primary
	public DataSource targetDataSource() {
		return DataSourceBuilder
				.create()
				.build();
	}

	@Bean(name = "targetInfoEntityManager")
	@Primary
	public LocalContainerEntityManagerFactoryBean targetInfoEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder
				.dataSource(targetDataSource())
				.properties(hibernateProperties())
				.packages(ResensysInfo.class)
				.persistenceUnit("targetInfoPU")
				.build();
	}

	@Bean(name = "targetInfoTransactionManager")
	@Primary
	public PlatformTransactionManager targetInfoTransactionManager(EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

	//DISINI LOH MAS
	@Bean(name = "targetHistoryEntityManager")
	public LocalContainerEntityManagerFactoryBean targetHistoryEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder
				.dataSource(targetDataSource())
				.properties(hibernateProperties())
				.packages(ResensysHistory.class)
				.persistenceUnit("targetHistoryPU")
				.build();
	}

	@Bean(name = "targetHistoryTransactionManager")
	public PlatformTransactionManager targetHistoryTransactionManager(EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

	//WES SELESAI

	//DISINI LOH MAS
	@Bean(name = "targetFormatEntityManager")
	public LocalContainerEntityManagerFactoryBean targetFormatEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder
				.dataSource(targetDataSource())
				.properties(hibernateProperties())
				.packages(ResensysFormat.class)
				.persistenceUnit("targetFormatPU")
				.build();
	}

	@Bean(name = "targetFormatTransactionManager")
	public PlatformTransactionManager targetFormatTransactionManager(EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

	//WES SELESAI

	/*@Bean(name = "targetDataEntityManager")
	public LocalContainerEntityManagerFactoryBean targetDataEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder
				.dataSource(targetDataSource())
				.properties(hibernateProperties())
				.packages(ResensysData.class)
				.persistenceUnit("targetDataPU")
				.build();
	}

	@Bean(name = "targetDataTransactionManager")
	public PlatformTransactionManager targetDataTransactionManager(@Qualifier("targetDataEntityManager") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}*/

	private Map<String, Object> hibernateProperties() {

		Resource resource = new ClassPathResource("hibernate.properties");

		try {
			Properties properties = PropertiesLoaderUtils.loadProperties(resource);
			return properties.entrySet().stream()
					.collect(Collectors.toMap(
							e -> e.getKey().toString(),
							e -> e.getValue())
					);
		} catch (IOException e) {
			return new HashMap<String, Object>();
		}
	}

	@Bean
	public TaskScheduler taskScheduler() {
		final ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
		scheduler.setPoolSize(10);
		return scheduler;
	}
}
