package com.spi.resensys.database.datatransfer.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spi.resensys.database.datatransfer.models.ResensysData;
import com.spi.resensys.database.datatransfer.models.ResensysHistory;
import com.spi.resensys.database.datatransfer.repositories.ResensysHistoryRepository;
import com.spi.resensys.database.datatransfer.repositories.ResensysInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class DataService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    @Qualifier("target")
    private JdbcTemplate target;

    @Autowired
    @Qualifier("source")
    private JdbcTemplate source;

    @Autowired
    private Environment environment;

    @Autowired
    private ResensysHistoryRepository resensysHistoryRepository;

    @Autowired
    private ResensysInfoRepository resensysInfoRepository;

    @Autowired
    private InfoService infoService;


    @PostConstruct
    private void initTable() {
        String[] datas = environment.getProperty("resensys.sensor.senimax.list", String[].class);

        for (String data : datas) {

            if (findLocalResensysDataTables("Data_" + data)) {
                logger.info("Senimax " + data + " data found on local database.");
            } else {

                if (findOnlineResensysDataTables("Data_" + data)) {
                    logger.info("Senimax " + data + " data found on resensys cloud database.");

                    String sql = "create table Data_" + data + "(siteID bigint(20), deviceID bigint(20), seqNo int(11), time timestamp, dataFormat int(11), value int(11), optional int(11), resensysID varchar(255), DOF varchar(255), COF varchar(255), realValue double)";
                    this.target.execute(sql);
                    logger.info("Table Data_" + data + " created.");

                } else {
                    logger.warn("Data " + data + " can't be found neither on local database nor on resensys cloud database.");
                    logger.info("Please change your 'resensys.sensor.senimax.list' value on application.properties with senimax number: " + data);
                }

            }

        }

        /*if (tableNames.isEmpty()) {

            tableNames = findOnlineResensysDataTables();

            if (tableNames.isEmpty()) {

                for (String data : datas) {
                    logger.warn("Data " + data + " can't be found neither on local database nor on resensys cloud database.");
                    logger.info("Please change your 'resensys.sensor.senimax.list' value on application.properties with senimax number: " + data);
                }

                System.exit(0);

            } else {

                for (String tableName : tableNames) {
                    String sql = "create table "+tableName+"(siteID bigint(20), deviceID bigint(20), seqNo int(11), time timestamp, dataFormat int(11), value int(11), optional int(11), resensysID varchar(255), DOF varchar(255), COF varchar(255), realValue double)";
                    this.target.execute(sql);
                    logger.info("Table " + tableName + " created.");
                }

            }

        } else {

            for (String tableName : tableNames) {
                logger.info(tableName + " found on local database.");
            }
        }*/
    }

    @Scheduled(fixedDelayString = "${resensys.sensor.data.service-delay}")
    public void scheduledDataService() {
        infoService.scheduledDataService();
        saveResensysData(environment.getProperty("resensys.sensor.data.limit", Integer.class));
    }

    private void saveResensysData(int limit) {
        String sql;
        this.source.setQueryTimeout(30000);

        List<String> tableNames = findLocalResensysDataTables();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S").withZone(ZoneId.systemDefault());

        for (String tableName : tableNames) {
            int saveCount = 0;

            LocalDateTime datetime = findLocalResensysDataTimeOrderByTime(tableName);
            logger.info("["+tableName+"]Last synchronized data: " + datetime.format(formatter));

            if (limit > 0) {
                sql = "select *, conv(DeviceID, 10, 16) as ResensysID from sensors." +tableName + " where Time >= ? order by Time desc limit " +limit;
            } else {
                sql = "select *, conv(DeviceID, 10, 16) as ResensysID from sensors." +tableName + " where Time >= ? order by Time desc";
            }

            List<Map<String, Object>> maps = this.source.queryForList(sql, datetime);

            for (Map map : maps) {
                ResensysData data = new ResensysData();
                data.setSiteID(Long.parseLong(map.get("SiteID").toString()));
                data.setDeviceID(Long.parseLong(map.get("DeviceID").toString()));
                data.setSeqNo(Integer.parseInt(map.get("SeqNo").toString()));
                data.setTime(LocalDateTime.from(formatter.parse(map.get("Time").toString())));
                data.setDataFormat(Integer.parseInt(map.get("DataFormat").toString()));
                data.setValue(Integer.parseInt(map.get("Value").toString()));
                data.setOptional(Integer.parseInt(map.get("Optional").toString()));
                data.setResensysID(getResensysID(map.get("ResensysID").toString()));
                saveCount += saveResensysData(tableName, data);
            }

            logger.info("Save " + saveCount + " data(s) to " + tableName);

            /*if (limit > 0) {
                sql = "select *,  concat(" +
                        "if(M1 like concat('%',DataFormat), '1', ''), " +
                        "if(M2 like concat('%',DataFormat), '2', ''), " +
                        "if(M3 like concat('%',DataFormat), '3', ''), " +
                        "if(m4 like concat('%',DataFormat), '4', ''), " +
                        "if(m5 like concat('%',DataFormat), '5', ''), " +
                        "if(m6 like concat('%',DataFormat), '6', ''), " +
                        "if(m7 like concat('%',DataFormat), '7', ''), " +
                        "if(m8 like concat('%',DataFormat), '8', ''), " +
                        "if(m9 like concat('%',DataFormat), '9', ''), " +
                        "if(m10 like concat('%',DataFormat), '10', '')" +
                        ") as channel FROM resensys.Data_"+ tableName +"  inner join resensys.Info on " +
                        "conv(Data_"+ tableName +".DeviceID , 10, 16) = convert(replace(Info.DID, '-','') using utf8) " +
                        "where Time >= ? order by Time asc limit " + limit;
            } else {
                sql = "select *,  concat(" +
                        "if(M1 like concat('%',DataFormat), '1', ''), " +
                        "if(M2 like concat('%',DataFormat), '2', ''), " +
                        "if(M3 like concat('%',DataFormat), '3', ''), " +
                        "if(m4 like concat('%',DataFormat), '4', ''), " +
                        "if(m5 like concat('%',DataFormat), '5', ''), " +
                        "if(m6 like concat('%',DataFormat), '6', ''), " +
                        "if(m7 like concat('%',DataFormat), '7', ''), " +
                        "if(m8 like concat('%',DataFormat), '8', ''), " +
                        "if(m9 like concat('%',DataFormat), '9', ''), " +
                        "if(m10 like concat('%',DataFormat), '10', '')" +
                        ") as channel FROM resensys.Data_"+ tableName +"  inner join resensys.Info on " +
                        "conv(Data_"+ tableName +".DeviceID , 10, 16) = convert(replace(Info.DID, '-','') using utf8) " +
                        "where Time >= ? order by Time asc";
            }*/

            /*List<Map<String, Object>> maps = this.target.queryForList(sql, datetime);
            int count = 1;

            for (Map map : maps) {
                String channel = map.get("channel").toString();
                int rawValue;
                Double DOF, COF,realValue;

                if (channel.equals("") || channel == null) {
                    DOF = 0.0;
                    COF = 0.0;
                    rawValue = 0;
                    realValue = 0.0;
                } else {
                    DOF = Double.parseDouble(map.get("DOF" +channel).toString());
                    COF = Double.parseDouble(map.get("COF" +channel).toString());
                    rawValue = (int) map.get("Value");
                    realValue = (rawValue - DOF) * COF;
                }

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                    Date lastReceivedData = sdf.parse(map.get("Time").toString());

                    ResensysHistory history = new ResensysHistory();
                    history.setSensorID(map.get("DID").toString());
                    history.setDataFormat((int) map.get("DataFormat"));
                    history.setLastReceivedData(lastReceivedData);
                    history.setRawValue(rawValue);
                    history.setRealValue(realValue);
                    history.setSenimax(map.get("SID").toString());
                    resensysHistoryRepository.saveAndFlush(history);

                    logger.info("["+count+"]" + mapper.valueToTree(history) + " saved");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                count++;

            }*/

            /*resensysDatas.addAll(this.source.query(sql, new BeanPropertyRowMapper(ResensysData.class), new Object[] {datetime}));

            for (ResensysData resensysData : resensysDatas) {
                saveCount += saveResensysData("Data_" + tableName, resensysData);
            }

            logger.info("Successfully save " + saveCount + " data(s).");*/

        }

        /*List<String> tableNames = findOnlineResensysDataTables();

        for (String tableName : tableNames) {

            int saveCount = 0;

            List<ResensysData> resensysDatas = new ArrayList<ResensysData>();

            Long localTime = findLocalResensysDataTimeOrderByTime(tableName);
            Date datetime = new Date(localTime);
            logger.info("["+tableName+"]Last synchronized data: " + datetime);

            if (limit > 0) {
                sql = "select * from "+ tableName +" where Time >= ? order by Time asc limit " + limit;
            } else {
                sql = "select * from " + tableName +" where Time >= ? order by Time asc";
            }

            this.source.setQueryTimeout(30000);

            resensysDatas.addAll(this.source.query(sql, new BeanPropertyRowMapper(ResensysData.class), new Object[] {datetime}));

            for (ResensysData resensysData : resensysDatas) {
                saveCount += saveResensysData(tableName, resensysData);
            }

            logger.info("Successfully save " + saveCount + " data(s).");

        }*/


    }

    private String getResensysID(String resensysID) {
        StringBuilder sb = new StringBuilder(resensysID);
        int offset = sb.length() - 2;
        while (offset > 0) {
            sb.insert(offset, "-");
            offset = offset - 2;
        }

        return sb.toString();
    }

    private LocalDateTime findLocalResensysDataTimeOrderByTime(String tableName) {
        LocalDateTime lastDatetime;
        String sql = "select Time from " + tableName + " order by Time desc limit 1";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S").withZone(ZoneId.systemDefault());
        LocalDateTime initDate = LocalDateTime.from(formatter.parse(environment.getProperty("resensys.sensor.data.init-date")));

        try {
            String input = this.target.queryForObject(sql, String.class);
            lastDatetime = LocalDateTime.from(formatter.parse(input));
        } catch (EmptyResultDataAccessException e) {
            lastDatetime = initDate;
        }

        return lastDatetime;
    }

    public List<String> findOnlineResensysDataTables() {
        String sql = "show tables from sensors";
        List<String> tableNames = new ArrayList<String>();
        List<Map<String, Object>> maps = this.source.queryForList(sql);
        String[] senimaxList = environment.getProperty("resensys.sensor.senimax.list", String[].class);

        for (Map map : maps) {

            String tableName = map.get("Tables_in_sensors").toString();

            for (String senimax : senimaxList) {

                if (tableName.equals("Data_" + senimax)) {
                    tableNames.add(tableName);
                }

            }
        }

        return tableNames;

    }

    public boolean findOnlineResensysDataTables(String tableName) {
        boolean exists = false;
        String sql = "show tables from sensors like ?";

        try {

            String table = this.source.queryForObject(sql, new Object[] {tableName}, String.class );

            if (table.equals(tableName)) {
                exists = true;
            }

        } catch (EmptyResultDataAccessException e) {
            logger.info(tableName + " is not found on resensys cloud database.");
        }

        return exists;
    }

    public List<String> findLocalResensysDataTables() {
        String sql = "show tables from resensys";
        List<String> tableNames = new ArrayList<String>();
        List<Map<String, Object>> maps = this.target.queryForList(sql);
        String[] senimaxList = environment.getProperty("resensys.sensor.senimax.list", String[].class);

        for (Map map : maps) {

            String tableName = map.get("Tables_in_resensys").toString();

            for (String senimax : senimaxList) {

                if (tableName.equals("Data_" + senimax)) {
                    tableNames.add(tableName);
                }

            }
        }

        return tableNames;
    }

    public boolean findLocalResensysDataTables(String tableName) {
        boolean exists = false;
        String sql = "show tables from resensys like ?";

        try {

            String table = this.target.queryForObject(sql, new Object[] {tableName}, String.class );

            if (table.equals(tableName)) {
                exists = true;
            }

        } catch (EmptyResultDataAccessException e) {
            logger.info(tableName + " is not found on local database.");
        }

        return exists;
    }

    private int saveResensysData(String tableName, ResensysData data) {

        int save;
        int channel = getDataChannel(data.getDataFormat(), data.getResensysID());

        if (channel > 0) {

            int RAW = data.getValue();
            String sql = "select COF"+ channel +", DOF"+ channel +", (("+RAW+" - DOF"+channel+") * COF"+channel+") as VAL from info where DID = '"+data.getResensysID()+"'";

            List<Map<String, Object>> rows = this.target.queryForList(sql);

            for (Map row : rows) {
                data.setCOF(String.valueOf(row.get("COF" + channel)));
                data.setDOF(String.valueOf(row.get("DOF" + channel)));
                data.setRealValue(Double.parseDouble(row.get("VAL").toString()));
            }

            LocalDateTime lastReceivedData = data.getTime();

            ResensysHistory history = new ResensysHistory();
            history.setSensorID(data.getResensysID());
            history.setSenimax(tableName);
            history.setLastReceivedData(lastReceivedData);
            history.setDataFormat(data.getDataFormat());
            history.setRawValue(data.getValue());
            history.setRealValue(data.getRealValue());

            Long max = lastReceivedData.plusHours(2L).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();

            Long last = lastReceivedData.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();

            if (last >= max) {
                history.setActive(false);
            }

            resensysHistoryRepository.save(history);
        }

        String sql = "insert into " + tableName + "(SiteID, DeviceID, SeqNo, Time, DataFormat, Value, Optional, ResensysID, COF, DOF, RealValue) values(?,?,?,?,?,?,?,?,?,?,?)";

        save = this.target.update(sql, data.getSiteID(), data.getDeviceID(), data.getSeqNo(), data.getTime(),
                data.getDataFormat(), data.getValue(), data.getOptional(), data.getResensysID(),
                data.getCOF(), data.getDOF(), data.getRealValue());

        return save;

    }

    private String getDID(Long deviceId) {
        String hex = Long.toHexString(deviceId);
        StringBuilder sb = new StringBuilder(hex);
        int offset = sb.length() - 2;
        while (offset > 0) {
            sb.insert(offset, "-");
            offset = offset - 2;
        }

        return sb.toString();
    }

    private int getDataChannel(int dataFormat, String DID) {
        try {
            String sql = "select concat(" +
                    "if(m1 like '%"+dataFormat+"', '1', ''), " +
                    "if(m2 like '%"+dataFormat+"', '2', ''), " +
                    "if(m3 like '%"+dataFormat+"', '3', ''), " +
                    "if(m4 like '%"+dataFormat+"', '4', ''), " +
                    "if(m5 like '%"+dataFormat+"', '5', ''), " +
                    "if(m6 like '%"+dataFormat+"', '6', ''), " +
                    "if(m7 like '%"+dataFormat+"', '7', ''), " +
                    "if(m8 like '%"+dataFormat+"', '8', ''), " +
                    "if(m9 like '%"+dataFormat+"', '9', ''), " +
                    "if(m10 like '%"+dataFormat+"', '10', '')" +
                    ") as data_format from info where DID = ?;";

            int channel =  this.target.queryForObject(sql, new Object[] {DID}, Integer.class);

            if (String.valueOf(channel).length() > 1) {
                String channels = String.valueOf(channel);
                channel = Integer.parseInt(channels.substring(1));
            }

            return channel;

        } catch (EmptyResultDataAccessException e) {
            return 0;
        }
    }
}
