package com.spi.resensys.database.datatransfer.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spi.resensys.database.datatransfer.models.ResensysFormat;
import com.spi.resensys.database.datatransfer.repositories.ResensysFormatRepository;
import com.spi.resensys.database.datatransfer.repositories.ResensysInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Service
public class FormatService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    @Qualifier("target")
    private JdbcTemplate target;

    @Autowired
    @Qualifier("source")
    private JdbcTemplate source;

    @Autowired
    private Environment environment;

    @Autowired
    private ResensysFormatRepository resensysFormatRepository;

    @Autowired
    private ResensysInfoRepository resensysInfoRepository;

    @Autowired
    private DataService dataService;

    private void init() {
        List<String> tableNames = dataService.findLocalResensysDataTables();
        logger.info("Table: " + mapper.valueToTree(tableNames));

        String sql;

        for (String tableName :tableNames) {
            sql = "SELECT DISTINCT DataFormat, CONV(DeviceID, 10, 16) AS DID FROM sensors.Data_"+ tableName +" order by Time desc;";

            List<Map<String, Object>> maps = this.source.queryForList(sql);

            int count =1;

            for (Map map : maps) {
                ResensysFormat format = new ResensysFormat();
                String resensysID = getResensysID(map.get("DID").toString());
                int dataFormat = Integer.parseInt(map.get("DataFormat").toString());
                format.setFormatID(map.get("DID").toString() + "-" + dataFormat);
                format.setResensysID(resensysID);
                format.setDataFormat(dataFormat);
                resensysFormatRepository.saveAndFlush(format);
                count++;
            }

            logger.info("Data format [" + tableName + "] " + (count - 1) + " record(s).");
        }
    }

    private String getResensysID(String DID) {
        StringBuilder sb = new StringBuilder(DID);
        int offset = sb.length() - 2;
        while (offset > 0) {
            sb.insert(offset, "-");
            offset = offset - 2;
        }

        return sb.toString();
    }
}
