package com.spi.resensys.database.datatransfer.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;

@Entity(name = "history")
@Table(name = "history")
public class ResensysHistory {

    @Id
    private String sensorID;

    @NotNull
    private String senimax;

    @NotNull
    private LocalDateTime lastReceivedData;

    @NotNull
    private Integer dataFormat;

    @NotNull
    private Integer rawValue;

    @NotNull
    private boolean active = true;

    private Double realValue;

    public String getSensorID() {
        return sensorID;
    }

    public void setSensorID(String sensorID) {
        this.sensorID = sensorID;
    }

    public String getSenimax() {
        return senimax;
    }

    public void setSenimax(String senimax) {
        this.senimax = senimax;
    }

    public LocalDateTime getLastReceivedData() {
        return lastReceivedData;
    }

    public void setLastReceivedData(LocalDateTime lastReceivedData) {
        this.lastReceivedData = lastReceivedData;
    }

    public Integer getDataFormat() {
        return dataFormat;
    }

    public void setDataFormat(Integer dataFormat) {
        this.dataFormat = dataFormat;
    }

    public Integer getRawValue() {
        return rawValue;
    }

    public void setRawValue(Integer rawValue) {
        this.rawValue = rawValue;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Double getRealValue() {
        return realValue;
    }

    public void setRealValue(Double realValue) {
        this.realValue = realValue;
    }
}
