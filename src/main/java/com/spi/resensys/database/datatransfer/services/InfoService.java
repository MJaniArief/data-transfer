package com.spi.resensys.database.datatransfer.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spi.resensys.database.datatransfer.models.ResensysInfo;
import com.spi.resensys.database.datatransfer.repositories.ResensysInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class InfoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    @Qualifier("target")
    private JdbcTemplate target;

    @Autowired
    @Qualifier("source")
    private JdbcTemplate source;

    @Autowired
    private ResensysInfoRepository infoRepository;

    @Autowired
    private Environment environment;

    public void scheduledDataService() {
        List<ResensysInfo> resultSet = findAllOnlineResensysInfo(0);

        for (ResensysInfo info : resultSet) {
            saveResensysInfo(info);
        }

        logger.info("Info table updated.");
    }

    public List<ResensysInfo> findAllOnlineResensysInfo(int limit) {
        String sql;

        if (limit > 0) {
            sql = "select * from resensys.Info limit " + limit;
        } else {
            sql = "select * from resensys.Info";
        }

        return this.source.query(sql, new BeanPropertyRowMapper(ResensysInfo.class));
    }

    public List<ResensysInfo> findAllLocalResensysInfo() {
        return infoRepository.findAll();
    }

    public ResensysInfo findOneOnlineResensysInfo(String infoId) {
        String sql = "SELECT * FROM resensys.Info where idInfo = ?";
        return (ResensysInfo) this.source.queryForObject(sql, new Object[] {infoId}, new BeanPropertyRowMapper(ResensysInfo.class));
    }

    public ResensysInfo findOneLocalResensysInfo(String infoId) {
        return infoRepository.findOne(infoId);
    }

    private void saveResensysInfo(ResensysInfo info) {
        infoRepository.save(buildResensysInfo(info));

        /*String id = info.getIdInfo();

        if (!findOneLocalResensysInfo(id).getIdInfo().equals(null)) {
            String sql = "insert into info(DID, TTL, DES, TYP, PFil, STT, SID, FirmVer, M1, DOF1, COF1, M2, DOF2, COF2, M3, DOF3, " +
                    "COF3, M4, DOF4, COF4, M5, DOF5, COF5, M6, DOF6, COF6, M7, DOF7, COF7, M8, DOF8, COF8, M9, DOF9, COF9, M10, " +
                    "DOF10, COF10, Last_Update, idInfo, Last_Changed_By) " +
                    "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            int save = this.target.update(sql, info.getDID(), info.getTTL(), info.getDES(), info.getTYP(), info.getPFil(), info.getSTT(),
                    info.getSID(), info.getFirmVer(), info.getM1(), info.getDOF1(), info.getCOF1(), info.getM2(), info.getDOF2(),
                    info.getCOF2(), info.getM3(), info.getDOF3(), info.getCOF3(), info.getM4(), info.getDOF4(), info.getCOF4(),
                    info.getM5(), info.getDOF5(), info.getCOF5(), info.getM6(), info.getDOF6(), info.getCOF6(),
                    info.getM7(), info.getDOF7(), info.getCOF7(), info.getM8(), info.getDOF8(), info.getCOF8(),
                    info.getM9(), info.getDOF9(), info.getCOF9(), info.getM10(), info.getDOF10(), info.getCOF10(),
                    info.getLast_Update(), info.getIdInfo(), info.getLast_Changed_By());

            if (save > 0) {
                logger.info("Resensys info data with id["+info.getIdInfo()+"] saved to local database.");
            }
        }*/
    }

    private ResensysInfo buildResensysInfo(ResensysInfo info) {
        ResensysInfo data = new ResensysInfo();

        data.setDID(info.getDID());
        data.setTTL(info.getTTL());
        data.setDES(info.getDES());
        data.setTYP(info.getTYP());
        data.setPFil(info.getPFil());
        data.setSTT(info.getSTT());
        data.setSID(info.getSID());
        data.setFirmVer(info.getFirmVer());
        data.setM1(info.getM1());
        data.setDOF1(info.getDOF1());
        data.setCOF1(info.getCOF1());
        data.setM2(info.getM2());
        data.setDOF2(info.getDOF2());
        data.setCOF2(info.getCOF2());
        data.setM3(info.getM3());
        data.setDOF3(info.getDOF3());
        data.setCOF3(info.getCOF3());
        data.setM4(info.getM4());
        data.setDOF4(info.getDOF4());
        data.setCOF4(info.getCOF4());
        data.setM5(info.getM5());
        data.setDOF5(info.getDOF5());
        data.setCOF5(info.getCOF5());
        data.setM6(info.getM6());
        data.setDOF6(info.getDOF6());
        data.setCOF6(info.getCOF6());
        data.setM7(info.getM7());
        data.setDOF7(info.getDOF7());
        data.setCOF7(info.getCOF7());
        data.setM8(info.getM8());
        data.setDOF8(info.getDOF8());
        data.setCOF8(info.getCOF8());
        data.setM9(info.getM9());
        data.setDOF9(info.getDOF9());
        data.setCOF9(info.getCOF9());
        data.setM10(info.getM10());
        data.setDOF10(info.getDOF10());
        data.setCOF10(info.getCOF10());
        data.setLast_Update(info.getLast_Update());
        data.setIdInfo(info.getIdInfo());
        data.setLast_Changed_By(info.getLast_Changed_By());

        return data;
    }

    /*public void updateResensysInfo(ResensysInfo info) {
        String id = info.getIdInfo();

        if (findOneLocalResensysInfo(id).getIdInfo().equals(id)) {
            String sql = "update info set DID = ?, TTL = ?, DES = ?, TYP = ?, PFil = ?, STT = ?, SID = ?, FirmVer = ?, " +
                    "M1 = ?, DOF1 = ?, COF1 = ?, M2 = ?, DOF2 = ?, COF2 = ?, M3 = ?, DOF3 = ?, COF3 = ?, " +
                    "M4 = ?, DOF4 = ?, COF4 = ?, M5 = ?, DOF5 = ?, COF5 = ?, M6 = ?, DOF6 = ?, COF6 = ?, " +
                    "M7 = ?, DOF7 = ?, COF7 = ?, M8 = ?, DOF8 = ?, COF8 = ?, M9 = ?, DOF9 = ?, COF9 = ?, " +
                    "M10 = ?, DOF10 = ?, COF10 = ?, Last_Update = ?, Last_Changed_By = ? where idInfo = ? ";
            int update = this.target.update(sql, info.getDID(), info.getTTL(), info.getDES(), info.getTYP(), info.getPFil(), info.getSTT(),
                    info.getSID(), info.getFirmVer(), info.getM1(), info.getDOF1(), info.getCOF1(), info.getM2(), info.getDOF2(),
                    info.getCOF2(), info.getM3(), info.getDOF3(), info.getCOF3(), info.getM4(), info.getDOF4(), info.getCOF4(),
                    info.getM5(), info.getDOF5(), info.getCOF5(), info.getM6(), info.getDOF6(), info.getCOF6(),
                    info.getM7(), info.getDOF7(), info.getCOF7(), info.getM8(), info.getDOF8(), info.getCOF8(),
                    info.getM9(), info.getDOF9(), info.getCOF9(), info.getM10(), info.getDOF10(), info.getCOF10(),
                    info.getLast_Update(), info.getLast_Changed_By(), info.getIdInfo());

            if (update > 0) {
                logger.info("Resensys info data with id["+info.getIdInfo()+"] updated to local database.");
            }
        }
    }*/


}
