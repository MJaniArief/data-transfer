package com.spi.resensys.database.datatransfer.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity(name = "format")
@Table(name = "format")
public class ResensysFormat {
    @Id
    private String formatID;

    @NotNull
    private String resensysID;

    @NotNull
    private int dataFormat;

    @NotNull
    private String description = "";

    public String getFormatID() {
        return formatID;
    }

    public void setFormatID(String formatID) {
        this.formatID = formatID;
    }

    public String getResensysID() {
        return resensysID;
    }

    public void setResensysID(String resensysID) {
        this.resensysID = resensysID;
    }

    public int getDataFormat() {
        return dataFormat;
    }

    public void setDataFormat(int dataFormat) {
        this.dataFormat = dataFormat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
