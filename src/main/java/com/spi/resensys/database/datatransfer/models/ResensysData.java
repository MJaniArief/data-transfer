package com.spi.resensys.database.datatransfer.models;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Date;

public class ResensysData {
    private Long siteID;
    private Long deviceID;
    private Integer seqNo;
    private LocalDateTime time;
    private Integer dataFormat;
    private Integer value;
    private Integer optional;
    private String resensysID;
    private String COF;
    private String DOF;
    private Double realValue;

    public Long getSiteID() {
        return siteID;
    }

    public void setSiteID(Long siteID) {
        this.siteID = siteID;
    }

    public Long getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(Long deviceID) {
        this.deviceID = deviceID;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Integer getDataFormat() {
        return dataFormat;
    }

    public void setDataFormat(Integer dataFormat) {
        this.dataFormat = dataFormat;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getOptional() {
        return optional;
    }

    public void setOptional(Integer optional) {
        this.optional = optional;
    }

    public String getResensysID() {
        return resensysID;
    }

    public void setResensysID(String resensysID) {
        this.resensysID = resensysID;
    }

    public String getCOF() {
        return COF;
    }

    public void setCOF(String COF) {
        this.COF = COF;
    }

    public String getDOF() {
        return DOF;
    }

    public void setDOF(String DOF) {
        this.DOF = DOF;
    }

    public Double getRealValue() {
        return realValue;
    }

    public void setRealValue(Double realValue) {
        this.realValue = realValue;
    }
}
