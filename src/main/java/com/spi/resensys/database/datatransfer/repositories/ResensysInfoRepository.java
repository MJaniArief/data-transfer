package com.spi.resensys.database.datatransfer.repositories;

import com.spi.resensys.database.datatransfer.models.ResensysInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ResensysInfoRepository extends JpaRepository<ResensysInfo, String> {
    @Query(value = "select replace(SID, '-', '') from info group by SID", nativeQuery = true)
    List<String> findAllSID();
}
