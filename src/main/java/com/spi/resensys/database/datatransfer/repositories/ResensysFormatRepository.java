package com.spi.resensys.database.datatransfer.repositories;

import com.spi.resensys.database.datatransfer.models.ResensysFormat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResensysFormatRepository extends JpaRepository<ResensysFormat, String> {
}
