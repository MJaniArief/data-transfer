package com.spi.resensys.database.datatransfer.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity(name = "info")
@Table(name = "info")
public class ResensysInfo {
    @NotNull
    @Column(name = "DID")
    private String DID;

    @NotNull
    @Column(name = "TTL")
    private String TTL;

    @NotNull
    @Column(name = "DES")
    private String DES;

    @NotNull
    @Column(name = "TYP")
    private String TYP;

    @NotNull
    @Column(name = "PFil")
    private String PFil;

    @NotNull
    @Column(name = "STT")
    private String STT;

    @NotNull
    @Column(name = "SID")
    private String SID;

    @NotNull
    @Column(name = "FirmVer")
    private String FirmVer;

    @NotNull
    @Column(name = "M1")
    private String M1;

    @NotNull
    @Column(name = "DOF1")
    private String DOF1;

    @NotNull
    @Column(name = "COF1")
    private String COF1;

    @NotNull
    @Column(name = "M2")
    private String M2;

    @NotNull
    @Column(name = "DOF2")
    private String DOF2;

    @NotNull
    @Column(name = "COF2")
    private String COF2;

    @NotNull
    @Column(name = "M3")
    private String M3;

    @NotNull
    @Column(name = "DOF3")
    private String DOF3;

    @NotNull
    @Column(name = "COF3")
    private String COF3;

    @NotNull
    @Column(name = "M4")
    private String M4;

    @NotNull
    @Column(name = "DOF4")
    private String DOF4;

    @NotNull
    @Column(name = "COF4")
    private String COF4;

    @NotNull
    @Column(name = "M5")
    private String M5;

    @NotNull
    @Column(name = "DOF5")
    private String DOF5;

    @NotNull
    @Column(name = "COF5")
    private String COF5;

    @NotNull
    @Column(name = "M6")
    private String M6;

    @NotNull
    @Column(name = "DOF6")
    private String DOF6;

    @NotNull
    @Column(name = "COF6")
    private String COF6;

    @NotNull
    @Column(name = "M7")
    private String M7;

    @NotNull
    @Column(name = "DOF7")
    private String DOF7;

    @NotNull
    @Column(name = "COF7")
    private String COF7;

    @NotNull
    @Column(name = "M8")
    private String M8;

    @NotNull
    @Column(name = "DOF8")
    private String DOF8;

    @NotNull
    @Column(name = "COF8")
    private String COF8;

    @NotNull
    @Column(name = "M9")
    private String M9;

    @NotNull
    @Column(name = "DOF9")
    private String DOF9;

    @NotNull
    @Column(name = "COF9")
    private String COF9;

    @NotNull
    @Column(name = "M10")
    private String M10;

    @NotNull
    @Column(name = "DOF10")
    private String DOF10;

    @NotNull
    @Column(name = "COF10")
    private String COF10;

    @NotNull
    @Column(name = "Last_Update")
    private String Last_Update;

    @NotNull
    @Column(name = "Last_Changed_By")
    private String Last_Changed_By;

    @Id
    @NotNull
    private String idInfo;

    public String getDID() {
        return DID;
    }

    public void setDID(String DID) {
        this.DID = DID;
    }

    public String getTTL() {
        return TTL;
    }

    public void setTTL(String TTL) {
        this.TTL = TTL;
    }

    public String getDES() {
        return DES;
    }

    public void setDES(String DES) {
        this.DES = DES;
    }

    public String getTYP() {
        return TYP;
    }

    public void setTYP(String TYP) {
        this.TYP = TYP;
    }

    public String getPFil() {
        return PFil;
    }

    public void setPFil(String PFil) {
        this.PFil = PFil;
    }

    public String getSTT() {
        return STT;
    }

    public void setSTT(String STT) {
        this.STT = STT;
    }

    public String getSID() {
        return SID;
    }

    public void setSID(String SID) {
        this.SID = SID;
    }

    public String getFirmVer() {
        return FirmVer;
    }

    public void setFirmVer(String firmVer) {
        FirmVer = firmVer;
    }

    public String getM1() {
        return M1;
    }

    public void setM1(String m1) {
        M1 = m1;
    }

    public String getDOF1() {
        return DOF1;
    }

    public void setDOF1(String DOF1) {
        this.DOF1 = DOF1;
    }

    public String getCOF1() {
        return COF1;
    }

    public void setCOF1(String COF1) {
        this.COF1 = COF1;
    }

    public String getM2() {
        return M2;
    }

    public void setM2(String m2) {
        M2 = m2;
    }

    public String getDOF2() {
        return DOF2;
    }

    public void setDOF2(String DOF2) {
        this.DOF2 = DOF2;
    }

    public String getCOF2() {
        return COF2;
    }

    public void setCOF2(String COF2) {
        this.COF2 = COF2;
    }

    public String getM3() {
        return M3;
    }

    public void setM3(String m3) {
        M3 = m3;
    }

    public String getDOF3() {
        return DOF3;
    }

    public void setDOF3(String DOF3) {
        this.DOF3 = DOF3;
    }

    public String getCOF3() {
        return COF3;
    }

    public void setCOF3(String COF3) {
        this.COF3 = COF3;
    }

    public String getM4() {
        return M4;
    }

    public void setM4(String m4) {
        M4 = m4;
    }

    public String getDOF4() {
        return DOF4;
    }

    public void setDOF4(String DOF4) {
        this.DOF4 = DOF4;
    }

    public String getCOF4() {
        return COF4;
    }

    public void setCOF4(String COF4) {
        this.COF4 = COF4;
    }

    public String getM5() {
        return M5;
    }

    public void setM5(String m5) {
        M5 = m5;
    }

    public String getDOF5() {
        return DOF5;
    }

    public void setDOF5(String DOF5) {
        this.DOF5 = DOF5;
    }

    public String getCOF5() {
        return COF5;
    }

    public void setCOF5(String COF5) {
        this.COF5 = COF5;
    }

    public String getM6() {
        return M6;
    }

    public void setM6(String m6) {
        M6 = m6;
    }

    public String getDOF6() {
        return DOF6;
    }

    public void setDOF6(String DOF6) {
        this.DOF6 = DOF6;
    }

    public String getCOF6() {
        return COF6;
    }

    public void setCOF6(String COF6) {
        this.COF6 = COF6;
    }

    public String getM7() {
        return M7;
    }

    public void setM7(String m7) {
        M7 = m7;
    }

    public String getDOF7() {
        return DOF7;
    }

    public void setDOF7(String DOF7) {
        this.DOF7 = DOF7;
    }

    public String getCOF7() {
        return COF7;
    }

    public void setCOF7(String COF7) {
        this.COF7 = COF7;
    }

    public String getM8() {
        return M8;
    }

    public void setM8(String m8) {
        M8 = m8;
    }

    public String getDOF8() {
        return DOF8;
    }

    public void setDOF8(String DOF8) {
        this.DOF8 = DOF8;
    }

    public String getCOF8() {
        return COF8;
    }

    public void setCOF8(String COF8) {
        this.COF8 = COF8;
    }

    public String getM9() {
        return M9;
    }

    public void setM9(String m9) {
        M9 = m9;
    }

    public String getDOF9() {
        return DOF9;
    }

    public void setDOF9(String DOF9) {
        this.DOF9 = DOF9;
    }

    public String getCOF9() {
        return COF9;
    }

    public void setCOF9(String COF9) {
        this.COF9 = COF9;
    }

    public String getM10() {
        return M10;
    }

    public void setM10(String m10) {
        M10 = m10;
    }

    public String getDOF10() {
        return DOF10;
    }

    public void setDOF10(String DOF10) {
        this.DOF10 = DOF10;
    }

    public String getCOF10() {
        return COF10;
    }

    public void setCOF10(String COF10) {
        this.COF10 = COF10;
    }

    public String getLast_Update() {
        return Last_Update;
    }

    public void setLast_Update(String last_Update) {
        Last_Update = last_Update;
    }

    public String getIdInfo() {
        return idInfo;
    }

    public void setIdInfo(String idInfo) {
        this.idInfo = idInfo;
    }

    public String getLast_Changed_By() {
        return Last_Changed_By;
    }

    public void setLast_Changed_By(String last_Changed_By) {
        Last_Changed_By = last_Changed_By;
    }
}
