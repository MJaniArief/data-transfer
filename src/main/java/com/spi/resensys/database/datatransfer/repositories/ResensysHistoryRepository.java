package com.spi.resensys.database.datatransfer.repositories;

import com.spi.resensys.database.datatransfer.models.ResensysHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface ResensysHistoryRepository extends JpaRepository<ResensysHistory, String> {
}
